import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

/* import screens */
import { SignupScreen } from './screens/signup.js'
import { LoginScreen } from './screens/login.js'
import { PhotosScreen } from './screens/photos.js'
import { ViewPhotoScreen } from './screens/viewPhoto.js'
import { SettingsScreen } from './screens/settings.js'

const Stack = createStackNavigator();

export default () => (
  <>
    <IconRegistry icons={EvaIconsPack} />
    <ApplicationProvider {...eva} theme={eva.light}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen name="signup" component={SignupScreen} />
          <Stack.Screen name="login" component={LoginScreen} />
          <Stack.Screen name="photos" component={PhotosScreen} />
          <Stack.Screen name="viewPhoto" component={ViewPhotoScreen} />
          <Stack.Screen name="settings" component={SettingsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </ApplicationProvider>
  </>
);

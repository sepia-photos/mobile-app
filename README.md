# Authentication Service

This is the mobile app for Sepia which uses [React Native](https://reactnative.dev/) with [Expo](https://expo.io/) and [UI Kitten](https://akveo.github.io/react-native-ui-kitten/). The web app for desktop will have a separate codebase, this repository is meant to be deployed to Android and iOS.

## Local Setup

Install node (the following uses nvm):

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash
    nvm install node # "node" is an alias for the latest version

Install Expo globally:

    npm install --global expo-cli

## Run the service

Running with expo:

    expo start

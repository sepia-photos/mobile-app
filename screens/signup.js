import { StatusBar } from 'expo-status-bar';
import React, { useRef } from 'react';
import { TouchableWithoutFeedback, View, Keyboard } from 'react-native';

import { Layout, Text, Input, Icon, Button } from '@ui-kitten/components';

const AlertIcon = (props) => (
  <Icon {...props} name='alert-circle-outline'/>
);

export const SignupScreen = ({ navigation }) => {
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const [message, setMessage] = React.useState('');

  const emailInput = useRef();
  const passwordInput = useRef();

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const handleSubmit = async function () {
    if (username == "") {
      setMessage("missing username");
      return;
    }
    if (password == "") {
      setMessage("missing password");
      return;
    }

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var body = JSON.stringify({
      username: username,
      password: password
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: body,
      redirect: 'follow'
    };

    //let res = await fetch("https://authentication-service-af277.ondigitalocean.app/signup", requestOptions);
    let res = await fetch("https://authentication-service-af277.ondigitalocean.app/signup", requestOptions);

    if (res.ok) {
      navigation.navigate('photos')
    } else {
      setMessage((await res.json()).message)
    }
  }

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 20}}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Text category='h1'>Create account</Text>
        </View>

        <View style={{flex: 1, width: '100%'}}>
          <Text
            style={{ textAlign: 'center', marginBottom: 5, height: (message == "") ? 0 : 'auto' }}
            category='s1'
            status="danger"
          >
            {message}
          </Text>

          <Input
            placeholder='Username'
            value={username}
            onChangeText={nextValue => setUsername(nextValue)}
            returnKeyType = {"next"}
            autoCorrect = {false}
            //onSubmitEditing={() => emailInput.current.focus()}
            onSubmitEditing={() => passwordInput.current.focus()}
            blurOnSubmit={false}
          />

          {/*<Input
            placeholder='Email (optional)'
            value={email}
            onChangeText={nextValue => setEmail(nextValue)}
            returnKeyType = {"next"}
            ref={emailInput}
            onSubmitEditing={() => passwordInput.current.focus()}
            blurOnSubmit={false}
          />*/}

          <Input
            value={password}
            placeholder='Password'
            accessoryRight={renderIcon}
            captionIcon={AlertIcon}
            caption='Should contain at least 8 characters'
            secureTextEntry={secureTextEntry}
            onChangeText={nextValue => setPassword(nextValue)}
            ref={passwordInput}
          />

          <Button
            style={{marginTop: 20, width: '100%'}}
            onPress={handleSubmit}
          >
            Sign up
          </Button>

          <View style={{flexDirection: 'row', width: '100%', marginTop: 20, justifyContent: 'center'}}>
            <Text category='s1' style={{textAlign: 'center'}}>Have an account? </Text>
            <Text category='s1' status='primary' onPress={() => navigation.navigate('login')}>Log in</Text>
          </View>
        </View>

        <StatusBar style="auto" />
      </Layout>
    </TouchableWithoutFeedback>
  );
}

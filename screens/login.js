import { StatusBar } from 'expo-status-bar';
import React, { useRef } from 'react';
import { TouchableWithoutFeedback, View, Keyboard } from 'react-native';

import { Layout, Text, Input, Icon, Button } from '@ui-kitten/components';

const AlertIcon = (props) => (
  <Icon {...props} name='alert-circle-outline'/>
);

export const LoginScreen = ({ navigation }) => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const passwordInput = useRef();

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 20}}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Text category='h1'>Welcome back!</Text>
        </View>

        <View style={{flex: 1, width: '100%'}}>
          <Input
            placeholder='Username'
            value={username}
            onChangeText={nextValue => setUsername(nextValue)}
            returnKeyType = {"next"}
            onSubmitEditing={() => passwordInput.current.focus()}
            blurOnSubmit={false}
          />

          <Input
            style={{ marginBottom: 15 }}
            value={password}
            placeholder='Password'
            accessoryRight={renderIcon}
            secureTextEntry={secureTextEntry}
            onChangeText={nextValue => setPassword(nextValue)}
            ref={passwordInput}
            onSubmitEditing={() => navigation.navigate('photos')}
          />

          <Button
            style={{marginTop: 20, width: '100%'}}
            onPress={() => navigation.navigate('photos')}
          >
            Log in
          </Button>

          <View style={{flexDirection: 'row', width: '100%', marginTop: 20, justifyContent: 'center'}}>
            <Text category='s1' style={{textAlign: 'center'}}>Don't have an account? </Text>
            <Text category='s1' status='primary' onPress={() => navigation.navigate('signup')}>Sign up</Text>
          </View>
        </View>

        <StatusBar style="auto" />
      </Layout>
    </TouchableWithoutFeedback>
  );
}

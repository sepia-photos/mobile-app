import { StatusBar } from 'expo-status-bar';
import React, { useRef } from 'react';
import { TouchableWithoutFeedback, View, Keyboard } from 'react-native';

import { Layout, Text, Input, Icon, Button, TopNavigation, TopNavigationAction } from '@ui-kitten/components';

const AlertIcon = (props) => (
  <Icon {...props} name='alert-circle-outline'/>
);

const BackIcon = (props) => (
  <Icon {...props} name='arrow-back'/>
);

export const SettingsScreen = ({ navigation }) => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const passwordInput = useRef();

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  const renderBackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={() => navigation.goBack()}/>
  );

  const renderTitle = (
    <Text category='h1'>Settings</Text>
  )

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Layout style={{flex: 1, alignItems: 'center', padding: 5}}>
        <View style={{flex: 1, width: '100%'}}>
        <TopNavigation
          style={{ marginTop: 50, marginBottom: 15, width: '100%'}}
          alignment='center'
          title={renderTitle}
          accessoryLeft={renderBackAction}
        />
        </View>

        <View style={{flex: 1, width: '100%'}}>
          <Button
            style={{marginTop: 20, marginLeft: 15, marginRight: 15, width: 'auto'}}
            appearance='outline'
            status='danger'
            onPress={() => navigation.navigate('login')}
          >
            Log out
          </Button>
        </View>

        <StatusBar style="auto" />
      </Layout>
    </TouchableWithoutFeedback>
  );
}

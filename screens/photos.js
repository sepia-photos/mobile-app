import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { TouchableWithoutFeedback, View, ScrollView, Keyboard, Image, Pressable, Dimensions } from 'react-native';

import { Layout, Text, Input, Icon, Button, TopNavigation, TopNavigationAction } from '@ui-kitten/components';

const DayOfPhotos = (props) => {
  return (
    <View>
      <Text style={{ margin: 10 }} category='h6'>{props.day}</Text>

      <View style={{flexDirection: 'row', flexWrap: 'wrap' }}>
        <Pressable onPress={() => {props.navigation.navigate('viewPhoto', { photo: require('../assets/test.jpg') })}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: 97, height: 97 }}
            source={require('../assets/test.jpg')}
          />
        </Pressable>
        <Pressable onPress={() => {props.navigation.navigate('viewPhoto', { photo: require('../assets/test3.jpg') })}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: 97, height: 97 }}
            source={require('../assets/test3.jpg')}
          />
        </Pressable>
        <Pressable onPress={() => {props.navigation.navigate('viewPhoto', { photo: require('../assets/test2.jpg') })}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: 97, height: 97 }}
            source={require('../assets/test2.jpg')}
          />
        </Pressable>
        <Pressable onPress={() => {props.navigation.navigate('viewPhoto', { photo: require('../assets/test.jpg') })}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: 97, height: 97 }}
            source={require('../assets/test.jpg')}
          />
        </Pressable>
        <Pressable onPress={() => {props.navigation.navigate('viewPhoto', { photo: require('../assets/test2.jpg') })}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: 97, height: 97 }}
            source={require('../assets/test2.jpg')}
          />
        </Pressable>
      </View>
    </View>
  )
}

const SettingsIcon = (props) => (
  <Icon {...props} name='settings-outline'/>
);

const UploadIcon = (props) => (
  <Icon {...props} name='cloud-upload-outline'/>
);

export const PhotosScreen = ({ navigation }) => {
  const renderSettingsAction = () => (
    <TopNavigationAction icon={SettingsIcon} onPress={() => navigation.navigate('settings')}/>
  );

  const renderUploadAction = () => (
    <TopNavigationAction icon={UploadIcon}/>
  );

  const renderTitle = (
    <Text category='h1'>Sepia</Text>
  )

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <ScrollView>
        <Layout style={{flex: 1, padding: 5, minHeight: Dimensions.get('window').height }}>
          <TopNavigation
            style={{ marginTop: 50, marginBottom: 15}}
            alignment='center'
            title={renderTitle}
            accessoryRight={renderSettingsAction}
            accessoryLeft={renderUploadAction}
          />

          <View style={{width: '100%'}}>
            <DayOfPhotos day="Today" navigation={navigation} />
            <DayOfPhotos day="Yesterday" navigation={navigation}/>
            <DayOfPhotos day="Mon, Dec 14" navigation={navigation}/>
            <DayOfPhotos day="Sun, Dec 13" navigation={navigation}/>
          </View>

          <StatusBar style="auto" backgroundColor="#fff" />

        </Layout>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}

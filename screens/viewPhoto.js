import { StatusBar } from 'expo-status-bar';
import React, { useRef } from 'react';
import { TouchableWithoutFeedback, View, Keyboard, Image, Dimensions } from 'react-native';

import { Layout, Text, Input, Icon, Button, TopNavigation, TopNavigationAction } from '@ui-kitten/components';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-back'/>
);

export const ViewPhotoScreen = ({ navigation, route }) => {
  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  const renderBackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={() => navigation.goBack()}/>
  );

  const renderTitle = (
    <Text category='h1'>Photo name</Text>
  )

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Layout style={{flex: 1, alignItems: 'center', padding: 5}}>
        <TopNavigation
          style={{ position: 'absolute', marginTop: 40, marginBottom: 15, width: '100%'}}
          accessoryLeft={renderBackAction}
        />

        <View style={{width: '100%', height: '100%', justifyContent: 'center'}}>
          <Image
            style={{ marginLeft: 1, marginBottom: 1, width: '100%', height: (Dimensions.get('window').height-240), resizeMode: 'contain' }}
            source={route.params.photo}
          />
        </View>

        <StatusBar style="auto" backgroundColor="#fff" />
      </Layout>
    </TouchableWithoutFeedback>
  );
}
